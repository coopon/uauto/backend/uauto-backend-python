# uauto-restapi-backend

RESTful API backend using Sanic python framework for union auto. Refer the [API documentation here](https://gitlab.com/coopon/uauto/uauto-systems-design/blob/master/REST_API.md).

**Note**: Python 3.5 (or) above is required. This is because sanic is written
using python's new async/await keyword introduced as a part of asyncio library.

**Pre - Requisites**

    pip install -r requirements.txt


**Steps to Setup and Start**

1. Make sure postgres is running.
2. Update `config.json` with db credentials to connect
3. `make upgrade`
4. `make run`
