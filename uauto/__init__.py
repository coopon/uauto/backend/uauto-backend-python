#!/usr/bin/env python3.6

import ujson
from sanic import Sanic

def create_app():

    app = Sanic(__name__)

    # load_configuration
    config = ujson.loads(open('config.json').read())
    db_settings = config['db']['dev']

    # setup and connect to database
    app.config.DB_HOST = db_settings['host']
    app.config.DB_USER = db_settings['username']
    app.config.DB_PORT = db_settings['port']
    app.config.DB_PASSWORD = db_settings['password']
    app.config.DB_DATABASE = db_settings['database']

    from uauto.database import db
    db.init_app(app)

    from uauto.views.user import UsersView, SingleUserView, login
    from uauto.views.vehicle import VehiclesView, SingleVehicleView
    from uauto.views.stand import StandsView, SingleStandView
    from uauto.views.union import UnionsView, SingleUnionView

    # API routing for users
    app.add_route(login, '/login', ['POST'])
    app.add_route(UsersView.as_view(), '/users')
    app.add_route(SingleUserView.as_view(), '/users/<_id:int>')

    # API routing for vehicles
    app.add_route(VehiclesView.as_view(), '/vehicles')
    app.add_route(SingleVehicleView.as_view(), '/vehicles/<reg_number>')

    # API routing for stands
    app.add_route(StandsView.as_view(), '/stands')
    app.add_route(SingleStandView.as_view(), '/stands/<name>')

    # API routing for unions
    app.add_route(UnionsView.as_view(), '/unions')
    app.add_route(SingleUnionView.as_view(), '/unions/<name>')

    app.run(host='127.0.0.1', port=7000, access_log=False)
