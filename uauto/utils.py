
def filter_fields(model_dict, ignore=[]):
    date_fields = ["created_at", "updated_at", "deleted_at", "full_check"]
    fmt = "%d-%m-%Y"

    if ignore:
        for field in ignore:
            model_dict.pop(field)

    for key, value in model_dict.items():
        if key in date_fields:
            if value:
                model_dict[key] = value.strftime(fmt)

    return model_dict
