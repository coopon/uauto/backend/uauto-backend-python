import bcrypt

from asyncpg import exceptions

from sanic.response import json, text
from sanic.views import HTTPMethodView

from uauto.models.user import User
from uauto.utils import filter_fields

'''
User
-------
  GET
   - all users
   - single user
  POST
    - create a new user
  PUT
    - update a user
  DELETE
    - remove a user
'''
class UsersView(HTTPMethodView):

    # return list of all users
    async def get(self, request):
        user_objects = await User.query.gino.all()
        ignore = ['password']
        users = [filter_fields(user.to_dict(), ignore) for user in user_objects]
        return json({"success": True, "users": users})

    # create a new user
    async def post(self, request):
        _mail = request.json.get('mail', None)
        _name = request.json.get('name', None)
        _mobile = request.json.get('mobile', None)
        _address = request.json.get('address', None)
        _license = request.json.get('license', None)
        _type = request.json.get('type', None)
        _password = request.json.get('password', None)

        # TODO: mobile and email needs confirmation check

        if _password:
            pwd = bytes(_password, encoding='utf-8')
            _password = bcrypt.hashpw(pwd, bcrypt.gensalt()).decode()

        try:
            user = await User.create(name=_name, mobile=_mobile, u_type=int(_type),
                                     mail=_mail, address=_address, license=_license,
                                     password=_password)
            return json({'success': True, 'user': user._id}, status=201)
        except exceptions.UniqueViolationError as uve:
            return json({'success': False, 'reason': f'{uve.constraint_name} has to be unique'}, status=400)


class SingleUserView(HTTPMethodView):

    # return single user
    async def get(self, request, _id):
        user = await User.get(_id)
        if user:
            return json({'success': True, 'user': filter_fields(user.to_dict(), ['password'])})
        else:
            return json({'success': False, 'reason': 'user not found', 'user': []}, status=404)

    # update details of a user
    async def put(self, request, _id):
        user = await User.get(int(_id))
        if user:
            _mail = request.json.get('mail', None)
            _name = request.json.get('name', None)
            _mobile = request.json.get('mobile', None)
            _address = request.json.get('address', None)
            _license = request.json.get('license', None)
            _type = request.json.get('type', None)
            _password = request.json.get('password', None)

            # TODO: mobile and email needs confirmation before getting updated

            update_query = User.update.values(mail=_mail, mobile=_mobile,
                                              name=_name, address=_address,
                                              license=_license, u_type=_type).where(User._id==int(_id))
            await update_query.gino.first()
            updated_user = await User.get(int(_id))
            return json({'success': True, 'user': filter_fields(updated_user.to_dict(), ['password'])}, status=200)
        else:
            return json({'success': False, 'reason': 'user not found', 'user': []}, status=404)

    # delete a user
    async def delete(self, request, _id):
        user = await User.get(int(_id))
        if user:
            await user.delete()
            message = 'deleted'
            status = 204
            success = True
        else:
            message = 'user not found'
            status = 404
            success = False
        return json({'success': success, 'reason': message}, status=status)


async def login(request):
    mail = request.json.get('mail', None)
    mobile = request.json.get('mobile', None)
    password = request.json.get('password', None)

    if not password:
        resp_data = {'success': False,
                     'reason': 'password not supplied'}
        status_code = 400
    else:
        if mobile:
            query = User.query.where(User.mobile==mobile)
        elif mail:
            query = User.query.where(User.mail==mail)

        user = await query.gino.first()
        if user:
            if bcrypt.checkpw(bytes(password, encoding='utf-8'), user.password.encode()):
                resp_data = {'success': True,
                             'user': filter_fields(user.to_dict(), ['password'])}
                status_code = 200
        else:
            resp_data = {'success': False,
                         'reason': 'user not found'}
            status_code = 404
    return json(resp_data, status=status_code)
