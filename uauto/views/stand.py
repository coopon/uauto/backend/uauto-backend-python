from sanic.response import json
from sanic.views import HTTPMethodView
from uauto.models.stand import Stand

'''
stand
-----
  GET
   - all stands
   - single stand
  POST
    - create a new stand
  PUT
    - update a stand
  DELETE
    - remove a stand

'''
class StandsView(HTTPMethodView):

    # return list of all stands
    async def get(self, request):
        return json({'hello': 'stands'})


class SingleStandView(HTTPMethodView):

    # return single stand
    async def get(self, request, name):
        return json({'method': request.method, 'name': name})

    # create a new stand
    async def post(self, request, name):
        return json({'method': request.method, 'name': name})

    # update details of a stand
    async def put(self, request, name):
        return json({'method': request.method, 'name': name})

    # delete a stand
    async def delete(self, request, name):
        return json({'method': request.method, 'name': name})

