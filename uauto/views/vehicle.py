from datetime import datetime

from sanic.response import json
from sanic.views import HTTPMethodView

from uauto.models.vehicle import Vehicle
from uauto.utils import filter_fields

'''
Vehicle
-------
  GET
   - all vehicles
   - single vehicle
  POST
    - create a new vehicle
  PUT
    - update a vehicle
  DELETE
    - remove a vehicle
'''
class VehiclesView(HTTPMethodView):

    # return list of all vehicles
    async def get(self, request):
        vehicle_objects = await Vehicle.query.gino.all()
        vehicles = [filter_fields(vehicle.to_dict(), []) for vehicle in vehicle_objects]
        return json({"success": True, "vehicles": vehicles})

    # register a new vehicle
    async def post(self, request):
        # format for date string
        fmt = "%d-%m-%Y"

        _reg_num = request.json.get('reg_number', None)
        _manu = request.json.get('manufacturer', None)
        _model = request.json.get('v_model', None)
        _fc = request.json.get('full_check', None)

        try:
            vehicle = await Vehicle.create(reg_number=_reg_num, manufacturer=_manu,
                                           v_model=_model, full_check=datetime.strptime(_fc, fmt))
            return json({'success': True, 'vehicle': vehicle.reg_number}, status=201)
        except exceptions.UniqueViolationError as uve:
            return json({'success': False, 'reason': f'{uve.constraint_name} has to be unique'}, status=400)


class SingleVehicleView(HTTPMethodView):

    # return single vehicle
    async def get(self, request, reg_number):
        vehicle = await Vehicle.query.where(Vehicle.reg_number==reg_number).gino.first()
        if vehicle:
            return json({'success': True, 'vehicle': filter_fields(vehicle.to_dict(), ['deleted_at'])})
        else:
            return json({'success': False, 'reason': 'vehicle not found', 'vehicle': []}, status=404)

    # update details of a vehicle
    async def put(self, request, reg_number):
        vehicle = await Vehicle.query.where(Vehicle.reg_number==reg_number).gino.first()
        if vehicle:
            fmt = "%d/%m/%Y"
            _fc = request.json.get('full_check', None)

            update_query = Vehicle.update.values(full_check=datetime.strptime(_fc, fmt))
            await update_query.gino.first()

            updated_vehicle = await Vehicle.query.where(Vehicle.reg_number==reg_number).gino.first()
            return json({'success': True, 'vehicle': filter_fields(updated_vehicle.to_dict(), ['deleted_at'])}, status=200)
        else:
            return json({'success': False, 'reason': 'vehicle not found', 'vehicle': []}, status=404)


    # delete a vehicle
    async def delete(self, request, reg_number):
        vehicle = await Vehicle.query.where(Vehicle.reg_number==reg_number).gino.first()
        if vehicle:
            await vehicle.delete()
            message = 'deleted'
            status = 204
            success = True
        else:
            message = 'vehicle not found'
            status = 404
            success = False
        return json({'success': success, 'reason': message}, status=status)
