from sanic.response import json
from sanic.views import HTTPMethodView

'''
Union
-----
  GET
   - all unions
   - single union
  POST
    - create a new union
  PUT
    - update a union
  DELETE
    - remove a union
'''
class UnionsView(HTTPMethodView):

    # return list of all unions
    async def get(self, request):
        return json({'hello': 'unions'})


class SingleUnionView(HTTPMethodView):

    # return single union
    async def get(self, request, name):
        return json({'method': request.method, 'name': name})

    # create a new union
    async def post(self, request, name):
        return json({'method': request.method, 'name': name})

    # update details of a union
    async def put(self, request, name):
        return json({'method': request.method, 'name': name})

    # delete a union
    async def delete(self, request, name):
        return json({'method': request.method, 'name': name})

