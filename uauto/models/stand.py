from uauto.database import db

class Stand(db.Model):
    __tablename__ = "stands"

    id = db.Column(db.BigInteger(), primary_key=True)
    lat = db.Column(db.String())
    lon = db.Column(db.String())
    name = db.Column(db.String())
    no_of_drivers = db.Column(db.Integer())
    operating_time = db.Column(db.String())
    rto = db.Column(db.String())
