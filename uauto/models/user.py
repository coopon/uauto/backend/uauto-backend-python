from datetime import datetime
from uauto.database import db

class User(db.Model):
    __tablename__ = "users"

    _id = db.Column(db.BigInteger(), primary_key=True)
    mobile = db.Column(db.String(length=13), unique=True, nullable=False)
    mail = db.Column(db.String(), nullable=True, unique=True)
    name = db.Column(db.String(), nullable=False)
    address = db.Column(db.String(), nullable=True)
    license = db.Column(db.String(), nullable=True, unique=True)
    password = db.Column(db.String(), nullable=True)
    u_type = db.Column(db.Integer(), nullable=True)
    created_at = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime(), nullable=True)
