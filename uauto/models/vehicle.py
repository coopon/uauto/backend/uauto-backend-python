from uauto.database import db
from datetime import datetime

class Vehicle(db.Model):
    __tablename__ = "vehicles"

    reg_number = db.Column(db.String(), primary_key=True)
    manufacturer = db.Column(db.String(), nullable=False)
    v_model = db.Column(db.String(), nullable=False)
    full_check = db.Column(db.DateTime(), nullable=False)
    created_at = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime(), nullable=True)

