from gino.ext.sanic import Gino

db = Gino()

from uauto.models.user import User
from uauto.models.vehicle import Vehicle
