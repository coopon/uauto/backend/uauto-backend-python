run:
	python3.6 manage.py run

revision:
	PYTHONPATH=. alembic revision --autogenerate;

upgrade:
	PYTHONPATH=. alembic upgrade head

downgrade:
	PYTHONPATH=. alembic downgrade head
